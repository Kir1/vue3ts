import axios from 'axios'
import {toast} from 'vue3-toastify';

const appAxios = axios.create({
    baseURL: import.meta.env.VITE_API_URL,
})

appAxios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error?.message) {
        toast.error(error.message);
    }
    return Promise.reject(error);
});

export default appAxios;