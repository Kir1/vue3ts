import {createRouter, createWebHistory} from 'vue-router'
import ItemsView from '@/views/ItemsView.vue'
import {usItemsStore} from "@/stores/items";
import {toast} from 'vue3-toastify';
import i18n from '@/plugins/i18n'

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            name: 'items',
            component: ItemsView,
            meta: {
                title: 'Welcome to the dashboard'
            }
        },
        {
            path: '/archive',
            name: 'archive',
            component: () => import('@/views/ArchiveView.vue'),
            meta: {
                title: 'Welcome to the dashboard121212'
            },
            beforeEnter: (to) => {
                const store = usItemsStore();
                if (store.itemsArchiveCount <= 0) {
                    toast.error(i18n.global.t('archive.dontPermission'));
                    return { path: '/'};
                }
            }
        },
        {
            path: '/test',
            name: 'test',
            component: () => import('@/views/TestView.vue'),
        }
    ]
})

export default router
