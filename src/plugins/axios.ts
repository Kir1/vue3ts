import type {App} from 'vue'
import appAxios from "@/helpers/axios";

export default {
    install: (app: App) => {
        app.config.globalProperties.$axios = appAxios;
    }
}