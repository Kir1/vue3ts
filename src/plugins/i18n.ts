import 'vue3-toastify/dist/index.css';
import ru from '@/languages/ru'
import en from '@/languages/en'
import {createI18n} from 'vue-i18n'

export default createI18n({
    locale: 'ru',
    messages: {ru, en},
    legacy: false,
})