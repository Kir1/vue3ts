import type {PiniaPluginContext} from 'pinia';

export function axiosPiniaPlugin(context: PiniaPluginContext) {
    const {$toast, $axios, $t} = context.app.config.globalProperties;
    return {
        $axios,
        $toast,
        $t
    }
}