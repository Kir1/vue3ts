import './assets/main.scss'

import App from './App.vue'
import {createApp} from 'vue'

const app = createApp(App)

import router from './router'

app.use(router)

import Vuetify from '@/plugins/vuetify'

app.use(Vuetify)

import i18n from '@/plugins/i18n'

app.use(i18n)

import axios from '@/plugins/axios'

app.use(axios)

import Vue3Toastify, {type ToastContainerOptions} from 'vue3-toastify';

app.use(Vue3Toastify, {autoClose: 3000} as ToastContainerOptions);

import {createPinia} from 'pinia'

const pinia = createPinia()
import {axiosPiniaPlugin} from './plugins/pinia'

pinia.use(axiosPiniaPlugin)
app.use(pinia)

app.mount('#app')
