import {describe, it, beforeEach, expect, vi} from 'vitest';
import {createTestingPinia} from '@pinia/testing';
import i18n from '@/plugins/i18n'
import router from '@/router'
import {mount} from '@vue/test-utils';
import TestView from '@/views/TestView.vue';
import {usItemsStore} from "@/stores/items";

describe('ItemsView', () => {
    let wrapper: any = null;
    let store: any = null;

    beforeEach(() => {
        wrapper = mount(TestView, {
            global: {
                plugins: [
                    i18n,
                    router,
                    createTestingPinia({
                        initialState: {
                            items: {
                                items: [{
                                    id: 1,
                                    name: '10',
                                    photo: '',
                                    photoFile: '',
                                    inArchive: false
                                }]
                            },
                        },
                        stubActions: false,
                        createSpy: vi.fn,
                    }),
                ]
            }
        })

        store = usItemsStore();
    })

    it('test init store', () => {
        store.addLocale(
            {
                id: 1,
                name: '10',
                photo: '',
                photoFile: '',
                inArchive: false
            },
            {
                id: 2,
                name: '10',
                photo: '',
                photoFile: '',
                inArchive: false
            }
        );
        expect(store.itemsCount).toEqual(2);
    })

    it('test render component', () => {
        expect(wrapper.text()).toContain('Предметы: 1Архив: 0')
    })
})
