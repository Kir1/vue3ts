import ItemForm from "@/components/item/ItemForm.vue";
import {computed, reactive} from "vue";
import {useI18n} from "vue-i18n";
import {toast} from 'vue3-toastify';
import {usItemsStore} from "@/stores/items";
import type {IItem, HTMLInputEvent} from "@/ts/interfaces";
import type {TItemFormRef} from "@/ts/types";

export default function useItemFormCreate(emit: Function) {
    const store = usItemsStore();

    const {t} = useI18n();

    const item = reactive<IItem>({
        name: '',
        photo: '',
        photoFile: null,
        inArchive: false,
    });

    const nameRules = [
        (value: string) => {
            if (value?.length > 2) return true;
            return computed(() => t('rules.min', {count: 3})).value;
        },
    ];

    const changePhoto = (item: IItem, event: HTMLInputEvent) => {
        const photoFile = event?.target?.files?.[0];
        if (photoFile) {
            item.photoFile = photoFile;
            const reader = new FileReader();
            reader.onload = (e) => {
                item.photo = e?.target?.result;
            }
            reader.readAsDataURL(photoFile);
        }
    }

    const submit = async (item: IItem, itemFormRef: TItemFormRef, storeMethod: Function, storeLocalMethod: Function, haveIdIncrement = true) => {
        const {valid} = await itemFormRef.value.form.validate();
        if (valid) {
            const id = haveIdIncrement ? Math.max(...store.itemsList.map(item => item.id as number)) + 1 : item.id;
            const itemData = {...item, id};
            // TO DO поскольку отправляем данные не-постгящему, получаем ошибку, а в итоге всё равно создаём
            storeMethod(itemData).finally(() => {
                storeLocalMethod(itemData);
                emit('close');
            });
        } else {
            toast.error(t('error.form'));
        }
    }

    return {
        nameRules,
        item,
        submit,
        ItemForm,
        changePhoto,
        store
    }
}
