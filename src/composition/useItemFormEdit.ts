import useItemFormCreate from "@/composition/useItemFormCreate";
import type {IItem} from "@/ts/interfaces";
import {reactive,} from "vue";
import {storeToRefs} from "pinia";

export default function useItemFormEdit(emit: Function, itemProp: { itemId: number }) {
    const form = useItemFormCreate(emit);
    const {getItem} = storeToRefs(form.store);
    const itemFromStore = getItem.value()(itemProp.itemId);
    const item = reactive<IItem>({...itemFromStore} as IItem);
    return {
        ...form,
        item,
    };
}
