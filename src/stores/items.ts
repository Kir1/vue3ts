import {defineStore} from 'pinia'
import type {IState, IItem, IItemOriginal} from '@/ts/interfaces'
import type {AxiosResponse} from "axios";
import appAxios from "@/helpers/axios";

const createFormData = (item: IItem) => {
    const formData = new FormData();
    formData.append('name', item.name);
    formData.append('inArchive', item.inArchive as unknown as string);
    formData.append('photo', item.photoFile as string | Blob);
    return formData;
}

const editFormData = (item: IItem) => {
    const formData = createFormData(item)
    formData.append('id', item.id as unknown as string);
    return formData;
}

export const usItemsStore = defineStore('items', {
    state: (): IState => ({items: [], loaded: true}),
    getters: {
        getItem: ({items}) => () => {
            return (id: number) => items.length ? items.find(item => item.id === id) : []
        },
        itemsList: ({items}) => items.length ? items.filter(item => item.inArchive === false) : [],
        itemsCount: ({items}) => items.length,
        itemsArchiveList: ({items}) => items.length ? items.filter(item => item.inArchive) : [],
        itemsArchiveCount() {
            return this.itemsArchiveList.length;
        },
    },
    actions: {
        async request(request: Function) {
            this.loaded = true;
            try {
                await request();
            } finally {
                this.loaded = false;
            }
        },
        init() {
            this.request(async () => {
                await appAxios.get<string, AxiosResponse<IItemOriginal[]>>("/albums/1/photos").then(
                    ({data}) => {
                        const items = data.map(item => ({
                            id: item.id,
                            name: item.title,
                            photo: item.thumbnailUrl,
                            photoFile: null,
                            inArchive: false
                        })).slice(0, 10);
                        items[0].photo = '';
                        this.items = items;
                    }
                )
            })
        },
        add(item: IItem) {
            return this.request(async () => {
                await appAxios.post<string, AxiosResponse<IItem>>("/posts", createFormData(item)).then(
                    ({data}) => this.items.push(data)
                )
            })
        },
        addLocale(item: IItem) {
            this.items.push(item);
        },
        remove(id: number) {
            this.request(async () => {
                await appAxios.post<string, AxiosResponse<IItem[]>>(`/posts/${id}`).then(
                    ({data}) => {
                        this.items = data.filter(item => item.id === id)
                    }
                );
            })
        },
        edit(item: IItem) {
            return this.request(async () => {
                await appAxios.put<string, AxiosResponse<IItem[]>>(`/posts/${item.id}`, editFormData(item)).then(
                    () => {
                        const foundItemIndex = this.items.findIndex(localItem => localItem.id === item.id);
                        if (foundItemIndex >= 0) {
                            this.items.splice(foundItemIndex, 1, item)
                        }
                    }
                )
            })
        },
        editLocale(item: IItem) {
            const foundItemIndex = this.items.findIndex((localItem => localItem.id === item.id));
            if (foundItemIndex >= 0) {
                this.items.splice(foundItemIndex, 1, item)
            }
        },
        archive(item: IItem) {
            const itemData = {...item, inArchive: true};
            return this.edit(itemData).finally(() => this.editLocale(itemData));
        },
        unarchive(item: IItem) {
            const itemData = {...item, inArchive: false};
            return this.edit(itemData).finally(() => this.editLocale(itemData));
        },
    },
})