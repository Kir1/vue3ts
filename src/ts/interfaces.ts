import type {ELanguages, EThemes} from "@/ts/enums";
import type {TFile, TFileBuffer} from "@/ts/types";

export interface IUserSettings {
    theme: EThemes,
    language: ELanguages,
}

export interface IUserSettings {
    theme: EThemes,
    language: ELanguages,
}

export interface IState {
    items: IItem[],
    loaded: boolean
}

export interface IItemOriginal {
    id: number,
    albumId: string,
    thumbnailUrl: string,
    title: string,
    url: string,
}

export interface IItem {
    id?: number,
    name: string,
    photo?: TFileBuffer,
    photoFile: TFile,
    inArchive?: boolean
}

export interface HTMLInputEvent extends Event {
    target: HTMLInputElement & EventTarget;
}