import type ItemForm from "@/components/item/ItemForm.vue";

export type TValueOrNull<T = any> = T | null | undefined;

export type TFile = TValueOrNull<Blob | string | File>;

export type TFileBuffer = TValueOrNull<string | ArrayBuffer>;

export type TItemFormRef = InstanceType<typeof ItemForm> | null;