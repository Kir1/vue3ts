export enum ELanguages {
    EN = 'en',
    RU = 'ru',
}

export enum EThemes {
    LIGHT = 'light',
    DARK = 'dark',
}

export enum EItemColor {
    LIGHT = 'black',
    DARK = 'grey',
}

export enum EItemStyle {
    NORMAL = 'normal',
    BOLD = 'bold',
}