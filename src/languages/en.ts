export default {
    main: {
        actions: 'Actions',
        action: 'Action',
        title: 'Title',
        photo: 'Photo',
        inArchive: 'In archive',
        creation: 'Creation',
        editing: 'Editing',
    },
    error: {
        form: 'Invalid fields',
    },
    rules: {
        min: 'Minimum {count}',
    },
    archive: {
        dontPermission: 'You dont have items in the archive.',
        hint: 'On this page you can see archived items.',
    },
    head: {
        items: 'Items: {count}',
        archive: 'Archive: {count}',
    },
    actions: {
        create: 'Create',
        edit: 'Edit',
        remove: 'Remove',
        archive: 'Archive',
        unarchive: 'Unarchive',
        selectAll: 'Select all'
    },
    theme: {
        theme: 'Theme: {theme}',
        dark: 'Dark',
        light: 'Light',
    },
}