export default {
    main: {
        actions: 'Действия',
        action: 'Действие',
        title: 'Название',
        photo: 'Фото',
        inArchive: 'В архиве',
        creation: 'Создание',
        editing: 'Редактирование',
    },
    error: {
        form: 'Поля невалидны',
    },
    rules: {
        min: 'Нужно минимум {count}',
    },
    archive: {
        dontPermission: 'У вас нет предметов в архиве.',
        hint: 'На данной странице вы можете увидеть архивированные предметы.',
    },
    head: {
        items: 'Предметы: {count}',
        archive: 'Архив: {count}',
    },
    actions: {
        create: 'Создать',
        edit: 'Редактировать',
        remove: 'Удалить',
        archive: 'Архивировать',
        unarchive: 'Разархивировать',
        selectAll: 'Выбрать все',
        action: 'Действие',
    },
    theme: {
        theme: 'Тема: {theme}',
        dark: 'Тёмная',
        light: 'Светлая',
    },
}