/// <reference types="vite/client" />

interface ImportMetaEnv {
    VITE_API_URL: string,
}

import { defineConfig } from "vite";
import vuetify from "vite-plugin-vuetify";

export default defineConfig({
    test: {
        globals: true,
        environment: "jsdom",
        setupFiles: "vuetify.config.js",
        deps: {
            inline: ["vuetify", "element-plus"],
        },
    },
});
